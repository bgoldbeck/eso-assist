-- First, we create a namespace for our addon by declaring a top-level table that will hold everything else.
EA = {}
 
-- This isn't strictly necessary, but we'll use this string later when registering events.
-- Better to define it in a single place rather than retyping the same string.
EA.name = "ESOAssist"

EA.staminaBear = "Wild Guardian"
EA.magickaBear = "Eternal Guardian"

EA.magickaBearUnsummonedId = 85986
EA.magickaBearSummonedId = 94625

EA.staminaBearUnsummonedId = 85990
EA.staminaBearSummonedId = 92163

EA.SLOT_ULTIMATE = ACTION_BAR_ULTIMATE_SLOT_INDEX + 1

EA.canUseOrbs = true

--Default Saved Variables.
EA.defaults = {
	prioitizeOrbs = false,
	debugMode = false,
	verboseOrbBlocking = true,
	
	noBearWarning = true,
	inCombatIndicator = false,
	noClanfearWarning = true,
	
	synergies = {
		["Charged Lightning"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_sorcerer_storm_atronach.dds|t"},
		["Blessed Shards"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_templar_sun_strike.dds|t"},
		["Holy Shards"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_templar_sun_strike.dds|t"},
		["Conduit"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_sorcerer_lightning_splash.dds|t"},
		["Harvest"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_warden_007.dds|t"},
		["Icy Escape"] = {category = "Class", enabled = false, icon = "|t35:35:/esoui/art/icons/ability_warden_005_b.dds|t"},
		["Combustion"] = {category = "Other", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_undaunted_004.dds|t"},
		["Healing Combustion"] = {category = "Other", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_undaunted_004b.dds|t"},
		["Grave Robber"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_necromancer_004_b.dds|t"},
		["Pure Agony"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_necromancer_010_b.dds|t"},
		["Blood Funnel"] = {category = "Other", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_undaunted_001.dds|t"},
		["Blood Feast"] = {category = "Other", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_undaunted_001_b.dds|t"},
		["Ignite"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_dragonknight_010.dds|t"},
		["Soul Leech"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_nightblade_018.dds|t"},
		["Hidden Refresh"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_nightblade_015.dds|t"},
		["Supernova"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_templar_solar_disturbance.dds|t"},
		["Gravity Crush"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_templar_solar_disturbance.dds|t"},
		["Purify"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_templar_extended_ritual.dds|t"},
		["Shackle"] = {category = "Class", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_dragonknight_006.dds|t"},
		["Radiate"] = {category = "Other", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_undaunted_002_b.dds|t"},
		["Bone Wall"] = {category = "Other", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_undaunted_005.dds|t"},
		["Spinal Surge"] = {category = "Other", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_undaunted_005b.dds|t"},
		["Sanguine Burst"]  = {category = "Other", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_u23_bloodball_chokeonit.dds|t"},
		["Destructive Outbreak"]  = {category = "Hel Ra Citadel", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_sorcerer_065.dds|t"},
		["Sigil of Sustain"] = {category = "Arena", enabled = true, icon = "|t35:35:/esoui/art/icons/sigil_speed_001.dds|t"},
		["Sigil of Resurrection"] = {category = "Arena", enabled = true, icon = "|t35:35:/esoui/art/icons/sigil_power_001.dds|t"},
		["Sigil of Defense"] = {category = "Arena", enabled = true, icon = "|t35:35:/esoui/art/icons/sigil_defense_001.dds|t"},
		["Sigil of Healing"] = {category = "Arena", enabled = true, icon = "|t35:35:/esoui/art/icons/sigil_healing_001.dds|t"},
		["Sigil of Power"] = {category = "Arena", enabled = true, icon = "|t35:35:/esoui/art/icons/sigil_power_001.dds|t"},
		["Sigil of Haste"] = {category = "Arena", enabled = true, icon = "|t35:35:/esoui/art/icons/sigil_speed_001.dds|t"},
		
		["Blade of Woe"] = {category = "Dark Brotherhood", enabled = true, icon = "|t35:35:/esoui/art/icons/achievement_darkbrotherhood_003.dds|t"},
		
		
		["Shake Off Spice"] = {category = "Ruins of Mazzatun", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_armor_003_a.dds|t"},
		["Gateway"] = {category = "Cloudrest", enabled = true, icon = "|t35:35:/esoui/art/icons/collectible_memento_pearlsummon.dds|t"},
		["Welkynar's Light"] = {category = "Cloudrest", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_templar_channeled_focus.dds|t"},
		["Shed Hoarfrost"] = {category = "Cloudrest", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_warden_006.dds|t"},
		["Time Breach"] = {category = "Sunspire", enabled = true, icon = "|t35:35:/esoui/art/icons/sigil_speed_001.dds|t"},
		["Remove Bolt"] = {category = "Sunspire", enabled = true, icon = "|t35:35:/esoui/art/icons/sigil_speed_001.dds|t"},
		
		["Remove Shield"] = {category = "Lair of Maarselok", enabled = true, icon = "|t35:35:/esoui/art/icons/crafting_alchemy_spore_pod.dds|t"},
		["Purify Scourge Seed"] = {category = "Lair of Maarselok", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_healer_017.dds|t"},
		
		["Brazier"] = {category = "Cradle of Shadows", enabled = true, icon = "|t35:35:/esoui/art/icons/quest_small_furnishing_001.dds|t"},
		
		["Purifying Blessing"] = {category = "Falkreath Hold", enabled = true, icon = "|t35:35:/esoui/art/icons/ability_warrior_028.dds|t"},
		
	
	},

}

local TimerTable = {}
function DelayedBuffer(key, buffer)
	if key == nil then 
		return 
	end
	if TimerTable[key] == nil then 
		TimerTable[key] = {} 
	end
	TimerTable[key].buffer = buffer or 0.2
	TimerTable[key].now = GetFrameTimeSeconds()
	
	if TimerTable[key].last == nil then 
		TimerTable[key].last = TimerTable[key].now 
	end
	
	TimerTable[key].diff = TimerTable[key].now - TimerTable[key].last
	TimerTable[key].result = TimerTable[key].diff >= TimerTable[key].buffer
	
	if TimerTable[key].result then 
		TimerTable[key].last = TimerTable[key].now 
	end
	return TimerTable[key].result
end


-- Next we create a function that will initialize our addon
function EA:Initialize()

	EA.savedVars = ZO_SavedVars:New('EA_SAVEDVARIABLES_DB' , 34, nil , EA.defaults)
	
	EVENT_MANAGER:RegisterForEvent(self.name, EVENT_PLAYER_COMBAT_STATE, EA_OnPlayerCombatState)
	ZO_PreHook("ZO_ActionBar_CanUseActionSlots", function()
  		flag = not flag -- Since ZO_ActionBar_CanUseActionSlots is called twice for each ability cast
		
		local slotNum = tonumber(debug.traceback():match('keybind = "ACTION_BUTTON_(%d)')) -- get pressed button
		-- break if consumable or empty slot
		if (slotNum == 9 or GetSlotBoundId(slotNum) == 0) then 
			return false 
		end 
		
		--d("slot number and type: " .. slotNum .. " " .. type(slotNum))
		if not EA_ShouldUseAbilityAgain(slotNum) then
			--d("Blocked")
			-- Darken the ability slot
			return false -- ESO won't run ability press if PreHook returns true
		else
			-- Reveal the ability slot
			return false
		end
		
	end)
	


	EA.BuildMenu()
	EA.SetupHookForSynergyBlocking()
	EVENT_MANAGER:RegisterForEvent("ESOAssist", EVENT_COMBAT_EVENT, EA.OnCombatEvent) -- Registers all combat events
end


function EA_OnUpdate(force)
	force = force or false
	if not DelayedBuffer("delayUpdate-ESOAssist", 0.25) then 
		if force == false then return end 
	end
	--d(GetSlotBoundId(EA.SLOT_ULTIMATE))
	
	if EA.savedVars == nil then
		--d("wut?")
		return
	end
	
	
	if EA.savedVars.noBearWarning then
		--d("should show bear")
		local ultimateId = GetSlotBoundId(EA.SLOT_ULTIMATE)
		--d(ultimateId)
		if ultimateId == EA.magickaBearUnsummonedId or ultimateId == EA.magickaBearSummonedId 
		or ultimateId == EA.staminaBearUnsummonedId or ultimateId == EA.staminaBearSummonedId then
			--d("found bear")
			-- No bear and no bear == Show bear.
			if (not EA_SelfFindActiveBuff(EA.staminaBear) and not EA_SelfFindActiveBuff(EA.magickaBear)) then
				ESOAssistBearIndicator:SetHidden(false)
				--d("not hidden")
			else
				ESOAssistBearIndicator:SetHidden(true)
				--d("hidden")
			end
		end
	else
		ESOAssistBearIndicator:SetHidden(true)
	end
	
end


function EA.OnCombatEvent(_, result, isError, abilityName, abilityGraphic, abilityActionSlotType, sourceName, sourceType, targetName, targetType, hitValue, powerType, damageType, log, sourceUnitId, targetUnitId, abilityId)
	--The following abilities can trigger it:
	--Necrotic Orb (Undaunted guild skill line)
	--Energy Orb (Undaunted guild skill line)
	--Spear Shards (Aedric Spear skill line [Templar])
	--Luminous Shards (Aedric Spear skill line [Templar])

	--Here are the ability IDs belonging to the effect names and their localized names (in german), printed by /script d(GetAbilityName(abilityId)):
	--[85434]: "Speerscherben / nekrotische Kugel CD"
	--[63512]: "Speerscherben / nekrotische Kugel CD"
	--[48052]: "Spear Shards / Necrotic Orb CD"
	--[95924]: "Speerscherben/nekrotische Kugel^N"
	
	--if string.find(targetName, GetUnitName("player")) then
	if result == 2245 and (abilityId == 48052 or abilityId == 85434 or abilityId == 63512 or abilityId == 95924) then
		-- Shards on cooldown
		EA.canUseOrbs = false
	end
	if result == 2250 and (abilityId == 48052 or abilityId == 85434 or abilityId == 63512 or abilityId == 95924) then
		EA.canUseOrbs = true
	end
		
	--else
	--end
	
	--d("-------------------------")
	--d("result: " .. result)
	--d("-------------------------")
    --d("abilityId: " .. abilityId)
	--d("abilityName: " .. GetAbilityName(abilityId))
	--d("sourceName: " .. sourceName)
	--d("targetName: " .. targetName)
	--d("sourceUnitId: " .. sourceUnitId)
	--d("targetUnitId: " .. targetUnitId)
	--d("Source Name: " .. sourceDisplayName or "")
	--d("Target Name: " .. targetDisplayName or "")
end

function EA.SetupHookForSynergyBlocking()
	ZO_PreHook(SYNERGY, 'OnSynergyAbilityChanged',
		function(self)
			local name, icon = GetSynergyInfo()
		
			
			if name and icon then
				if EA.savedVars.synergies[name] == nil and EA.savedVars.debugMode then 
					d("ESOAssist Found New Synergy!\nname='" .. name .. "'\nicon='" .. icon .. "'\nPlease report to @MeanMisterMustard")
				elseif EA.savedVars.prioitizeOrbs == true then
					if EA.canUseOrbs == true then
						-- If orb cooldown is back up, then we want to prioritize them. 
						if (name ~= "Blessed Shards" and name ~= "Combustion" and 
							name ~= "Healing Combustion" and name ~= "Holy Shards" and
							name ~= "Charged Lightning") then
							-- Add these exceptions
							if EA.savedVars.synergies[name]["category"] == "Class" or
							   EA.savedVars.synergies[name]["category"] == "Other" then
							   if EA.savedVars.verboseOrbBlocking == true then
									d("Blocked [" .. name .. "] because of orb priority")
								end
								SHARED_INFORMATION_AREA:SetHidden(SYNERGY, true)
								return true
							end
						end
					end
				end
				
				
				if EA.savedVars.synergies[name] ~= nil and EA.savedVars.synergies[name]["enabled"] == false then
					--d("ESOAssist Blocking Synergy'" .. name .. "'")
					SHARED_INFORMATION_AREA:SetHidden(SYNERGY, true)
					return true
				end
			end
		end
	)
	
end

-- Then we create an event handler function which will be called when the "addon loaded" event
-- occurs. We'll use this to initialize our addon after all of its resources are fully loaded.
function EA.OnAddOnLoaded(event, addonName)
  -- The event fires each time *any* addon loads - but we only care about when our own addon loads.
	if addonName == EA.name then
		EA:Initialize()
	end
  
end
 
 function EA_OnPlayerCombatState(event, inCombat)
	--d(inCombat)
	--d("Hello - ESO ASSIST")
	--if (inCombat and true) == false then 
	if true == false then
		local bagToScan = SHARED_INVENTORY:GenerateFullSlotData(nil, BAG_BACKPACK)
		for index, slot in pairs(bagToScan) do
			local icon, stack, sellPrice, meetsUsageRequirement, locked, equipType, itemStyleId, quality = GetItemInfo(BAG_BACKPACK, slot.slotIndex)
			local link = GetItemLink(BAG_BACKPACK, slot.slotIndex)
			if link ~= "" then
				d("That item is a " .. link .. "!")
			end

			local name = GetItemName(BAG_BACKPACK, slot.slotIndex)
			local isItemBound = IsItemBound (BAG_BACKPACK, slot.slotIndex)
			--d("index: " .. index)
			d("name: " .. name)
			--d("icon: " .. icon)
			--d("slotIndex: " .. slot.slotIndex)
			d("equipType: " .. equipType)
			--d("isItemBound: " .. tostring(isItemBound))
			if (isItemBound == false and 
				equipType ~= EQUIP_TYPE_POISON and 
				equipType ~= EQUIP_TYPE_COSTUME) then
				if equipType ~= 0 then
					--d("TRYING TO EQUIP ITEM: " .. name)
					EquipItem(BAG_BACKPACK, slot.slotIndex)
				end
			end
			
		end
	end
	
    -- ...and then update the control.
	if (EA.savedVars.inCombatIndicator) then
		ESOAssistCombatIndicator:SetHidden(not inCombat)
	end

end

function EA_TargetHasBuff(slotAbilityName)
	local found = false
	--d("Checking target buffs, count=" .. GetNumBuffs('reticleover'))
	--d("slotName type in function pass: " .. type(tostring(slotAbilityName)))
	--d("Match with: " .. tostring(slotAbilityName))
	for i = 0, GetNumBuffs('reticleover'), 1 do
		local buffName, timeStarted, timeEnding, buffSlot, stackCount, iconFilename, buffType, effectType, abilityType, statusEffectType, abilityId, canClickOff, castByPlayer  = GetUnitBuffInfo("reticleover", i)	
		
		--d("target buff: " .. buffName .. " " .. i)
		if buffName == slotAbilityName and castByPlayer == true then
		
			--Remaining time.
			local remaining = timeEnding - (GetGameTimeMilliseconds() / 1000)
			if remaining > 5 then -- Some leeway, we will recast if the buff is about to go down
				found = true
				break
			end
		end		
	end
	return found
end

function EA_SelfFindActiveBuff(search)
	for i = 0, GetNumBuffs("player"), 1 do
		local buffName, timeStarted, timeEnding, buffSlot, stackCount, textureName, iconFilename, buffType, effectType, abilityType, statusEffectType, abilityId, canClickOff, castByPlayer = GetUnitBuffInfo("player", i)
		if buffName == search then
			return true
		end
	end
	return false
end

function EA_ShouldUseAbilityAgain(slotNum)
	local shouldUseAbility = true
	local slotName = GetSlotName(slotNum)
	if EA_TargetHasBuff(slotName) then
		--d("target has the buff")
		shouldUseAbility = false
	end
	return shouldUseAbility
end

-- Finally, we'll register our event handler function to be called when the proper event occurs.
EVENT_MANAGER:RegisterForEvent(EA.name, EVENT_ADD_ON_LOADED, EA.OnAddOnLoaded)