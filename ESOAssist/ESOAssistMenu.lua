local FAB = FancyActionBar
local LAM = LibAddonMenu2


function EA.BuildMenu()

	local panel = {
		type = 'panel',
		name = 'ESOAssist',
		displayName = 'ESOAssist',
		author = '|c0FF300@Rando|r',
		version = string.format('|c00FF00%s|r', "r1"),
		donation = '',
		registerForRefresh = true,
	}
	
	local options = {
		{
			type = "header",
			name = "|cFFFACDESOAssist|r",
		},
	}
	
	
	table.insert(options, 
		{
			type = "checkbox",
			name = "Enable In Combat Indicator",
			tooltip = "Show an icon while in combat?",
			getFunc = function() return EA.savedVars.inCombatIndicator end,
			setFunc = function(value)
				EA.savedVars.inCombatIndicator = value
			end,
		}
	)
	
	table.insert(options, 
		{
			type = "checkbox",
			name = "Enable Debug Mode",
			tooltip = "",
			getFunc = function() return EA.savedVars.debugMode end,
			setFunc = function(value)
				EA.savedVars.debugMode = value
			end,
		}
	)
	
	table.insert(options, 
		{
			type = "header",
			name = "|cFFFACD " .. "Pets" .. "|r",
		}
	)
	
	table.insert(options, 
		{
			type = "checkbox",
			name = "Show No Bear Warning",
			tooltip = "On screen icon for no bear pet summoned.",
			getFunc = function() return EA.savedVars.noBearWarning end,
			setFunc = function(value)
				EA.savedVars.noBearWarning = value
			end,
		}
	)
	
	table.insert(options, 
		{
			type = "header",
			name = "|cFFFACD " .. "Synergies" .. "|r",
		}
	)
	
	table.insert(options, 
		{
			type = "checkbox",
			name = "Prioritize Orbs / Shards",
			tooltip = "Take Shards / Orbs over other synergies",
			getFunc = function() return EA.savedVars.prioitizeOrbs end,
			setFunc = function(value)
				EA.savedVars.prioitizeOrbs = value
			end,
		}
	)
	
	table.insert(options, 
		{
			type = "checkbox",
			name = "Verbose Prioritize Orbs / Shards",
			tooltip = "Messages in chat for blocking Orbs over other synergies",
			getFunc = function() return EA.savedVars.verboseOrbBlocking end,
			setFunc = function(value)
				EA.savedVars.verboseOrbBlocking = value
			end,
		}
	)
	
	
	local category = ""
	
	for k, v in EA_Spairs(EA.savedVars.synergies, function(t,a,b) return t[a]["category"] < t[b]["category"] end) do
		if v["category"] ~= category then
			category = v["category"]
				table.insert(options, 
					{
						type = "header",
						name = "|cFFFACD " .. category .. "|r",
					}
				)
		end
		table.insert(options, 
			{
				type = "checkbox",
				name = EA.savedVars.synergies[k].icon .. " : " .. k,
				tooltip = k .. " Synergy On/Off",
				getFunc = function() return EA.savedVars.synergies[k]["enabled"] end,
				setFunc = function(value)
					EA.savedVars.synergies[k]["enabled"] = value
				end,
			}
		)
	end

	local name = "ESOAssist" .. 'Menu'
    LAM:RegisterAddonPanel(name, panel)
    LAM:RegisterOptionControls(name, options)
	
end

function EA_Spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys 
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end