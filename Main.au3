#include <SendMessage.au3>
#include <guiconstants.au3>
;#include <GUIConstantsEx.au3>
;#include <WindowsConstants.au3>

#include "AutoIT_ImageSearch/_ImageSearch.au3"
#include "AutoIT_ImageSearch/_ImageSearch_Debug.au3"
#include "GUIFactory.au3"
#include "AutoSynergyTab.au3"
#include "ConfigTab.au3"

#RequireAdmin

Opt("GUIOnEventMode", 1) ; Change to OnEvent mode
Global $elderScrollsTitle = "Elder Scrolls Online"
Global $begin = TimerInit()
Global $timerDiff
Global $logTextColor = 0xFF8000


;D.SynergyData = {
;    [108782]    = { cooldown = 20,   group = BLOOD_SYNERGY },                -- Blood Funnel Synergy     (Blood Altar)
;    [108787]    = { cooldown = 20,   group = BLOOD_SYNERGY },                -- Blood Feast Synergy      (Overflowing Altar)
;    [108788]    = { cooldown = 20,   group = TRAPPING_SYNERGY },             -- Spawn Broodlings Synergy (Trapping Webs)
;    [108791]    = { cooldown = 20,   group = TRAPPING_SYNERGY },             -- Black Widows Synergy     (Shadow Silk)
;    [108792]    = { cooldown = 20,   group = TRAPPING_SYNERGY },             -- Arachnophobia Synergy    (Tangling Webs)
;    [108793]    = { cooldown = 20,   group = RADIATE_SYNERGY },              -- Radiate Synergy          (Inner Fire)
;    [108794]    = { cooldown = 20,   group = BONE_SYNERGY },                 -- Bone Wall Synergy        (Bone Shield)
;    [108797]    = { cooldown = 20,   group = BONE_SYNERGY },                 -- Spinal Surge Synergy     (Bone Surge)
;    [108799]    = { cooldown = 20,   group = COMBUSTION_SYNERGY },           -- Combustion Synergy       (Necrotic Orb)
;    [108802]    = { cooldown = 20,   group = COMBUSTION_SYNERGY },           -- Combustion Synergy       (Energy Orb)
;    [108821]    = { cooldown = 20,   group = COMBUSTION_SYNERGY },           -- Holy Shards Synergy      (Luminous Shards)
;    [108924]    = { cooldown = 20,   group = COMBUSTION_SYNERGY },           -- Blessed Shards Synergy   (Spear Shards)
;    [108607]    = { cooldown = 20,   group = CONDUIT_SYNERGY },              -- Conduit Synergy          (Lightning Splash)
;    [108826]    = { cooldown = 20,   group = HEALING_SYNERGY },              -- Harvest Synergy          (Healing Seed)
;    [108824]    = { cooldown = 20,   group = PURGE_SYNERGY },                -- Purge Synergy            (Extended Ritual)
;    [102321]    = { cooldown = 20,   group = ATRONACH_SYNERGY },             -- Charged Lightning        (Summon Storm Atronach)
;    [108805]    = { cooldown = 20,   group = SHACKLE_SYNERGY },              -- Shackle Synergy          (Dragonknight Standard)
;    [108807]    = { cooldown = 20,   group = IGNITE_SYNERGY },               -- Ignite Synergy           (Dark Talons)
;    [108822]    = { cooldown = 20,   group = NOVA_SYNERGY },                 -- Supernova Synergy        (Nova)
;    [108823]    = { cooldown = 20,   group = NOVA_SYNERGY },                 -- Gravity Crush Synergy    (Supernova)
;    [108808]    = { cooldown = 20,   group = HIDDEN_REFRESH_SYNERGY },       -- Hidden Refresh Synergy   (Consuming Darkness)
;    [108814]    = { cooldown = 20,   group = SOUL_LEECH_SYNERGY },           -- Soul Leech Synergy       (Soul Shred)
;    [115567]    = { cooldown = 20,   group = GRAVE_ROBBER_SYNERGY },         -- Boneyard Synergy         (Grave Robber)
;    [115571]    = { cooldown = 20,   group = GRAVE_ROBBER_SYNERGY },         -- Avid Boneyard Synergy    (Grave Robber)
;    [118610]    = { cooldown = 20,   group = PURE_AGONY_SYNERGY },           -- Agony Totem Synergy      (Pure Agony)



;D.SynergyTexture = {
;    [COMBUSTION_SYNERGY]     = "/esoui/art/icons/ability_undaunted_004b.dds",
;    [CONDUIT_SYNERGY]        = "/esoui/art/icons/ability_sorcerer_liquid_lightning.dds",
;    [PURGE_SYNERGY]          = "/esoui/art/icons/ability_templar_extended_ritual.dds",
;    [HEALING_SYNERGY]        = "/esoui/art/icons/ability_warden_007.dds",
;    [BONE_SYNERGY]           = "/esoui/art/icons/ability_undaunted_005b.dds",
;    [BLOOD_SYNERGY]          = "/esoui/art/icons/ability_undaunted_001_b.dds",
;    [TRAPPING_SYNERGY]       = "/esoui/art/icons/ability_undaunted_003_b.dds",
;    [RADIATE_SYNERGY]        = "/esoui/art/icons/ability_undaunted_002_b.dds",
;    [ATRONACH_SYNERGY]       = "/esoui/art/icons/ability_sorcerer_storm_atronach.dds",
;    [SHACKLE_SYNERGY]        = "/esoui/art/icons/ability_dragonknight_006.dds",
;    [IGNITE_SYNERGY]         = "/esoui/art/icons/ability_dragonknight_010.dds",
;    [NOVA_SYNERGY]           = "/esoui/art/icons/ability_templar_solar_disturbance.dds",
;    [HIDDEN_REFRESH_SYNERGY] = "/esoui/art/icons/ability_nightblade_015.dds",
;    [SOUL_LEECH_SYNERGY]     = "/esoui/art/icons/ability_nightblade_018.dds",
;    [GRAVE_ROBBER_SYNERGY]   = "/esoui/art/icons/ability_necromancer_004_b.dds",
;    [PURE_AGONY_SYNERGY]     = "/esoui/art/icons/ability_necromancer_010_b.dds",

Global $appName = "ESO Assist"
If _Singleton($appName,1) = 0 Then
    Exit
EndIf

Global $showLogMessage = False

Global $stdBtnHeight = 30
Global $stdBtnWidth = 100

Global $inCombatIndicatorPath = @ScriptDir & "\Images\in_combat_indicator.bmp"

Global $logHwnd = GUICreate("Log Text Region",400,50,(@DesktopWidth / 2) - 200, 100,$WS_POPUP,BitOR($WS_EX_TOPMOST,$WS_EX_TOOLWINDOW))
GUISetBkColor($logTextColor)


Global $hGUI = GUICreate($appName, $guiWidth, $guiHeight,-1,-1)

Global $guiTab = GUICtrlCreateTab(0, 0, $guiWidth, $guiHeight)
Global $mainTab = GUICtrlCreateTabItem("Main")



Global $logTextRegion = CreateTextRgn($logHwnd,"ESO Assist!",50,"Arial",1000)
SetWindowRgn($logHwnd,$logTextRegion)


;Buttons
$iClose = _GUIFCreateButton("Close", ($guiWidth / 2) - ($stdBtnWidth / 2), $guiHeight - $stdBtnHeight, $stdBtnWidth, $stdBtnHeight)
GUICtrlSetOnEvent($iClose, "ClosePressed")
GUISetOnEvent($GUI_EVENT_CLOSE, "ClosePressed")



$autoSynergyTab =_CreateAutoSynergyTab()
$configTab = _CreateConfigTab()

GUISetState(@SW_SHOW, $logHwnd)
Sleep(100)
GUISetState(@SW_HIDE, $logHwnd)

Main()

Func Update()

   Local $inCombat = _ImageSearch($inCombatIndicatorPath, $imageSearchTolerance)
   Local $synergyItems = $synergyImages.Items

   If _IsChecked($autoAFKCheckbox) And $inCombat == false Then
	  If TimerDiff($begin) > (180 * 1000) Then
		 $begin = TimerInit()
		 ControlSend($elderScrollsTitle, "", "", $afkKey) ; Use Synergy
	  EndIf
   EndIf


   If $showLogMessage == True Then
	  If TimerDiff($begin) > 3000 Then
		 GUISetState(@SW_HIDE, $logHwnd)
		 $begin = TimerInit()
		 $showLogMessage = False
	  EndIf
   EndIf

   If WinActive($elderScrollsTitle, "") Then
	  ; Elder Scrolls Window active.
	  If $inCombat[0] == 1 Then
		;ControlClick($elderScrollsTitle, "", "", "left") ; Light Attack
		;Sleep(10)
	  EndIf

	  ;Check to use any synergies.
	  For $i = 0 To $synergyImages.Count -1
		 If _IsChecked($synergyCheckboxes[$i]) Then
			Local $return = _ImageSearch($synergyItems[$i], $imageSearchTolerance)
			If $return[0] = 1 Then
			   For $j = 0 To 2
				  ControlSend($elderScrollsTitle, "", "", $useSynergyKey) ; Use Synergy
				  ;_SetLogMessage("Used Synergy " & $synergyItems[$i])
				  Sleep(10)
			   Next
			EndIf
		 EndIf
	  Next

	  ; Check if we need to use weapon power potions.
	  If _IsChecked($usePotionsCheckbox) Then
		 If $inCombat[0] = 1 Then

			; We are in combat
			For $j = 0 To UBound($potionPath) - 1
			   Local $potionCooldown = _ImageSearch($potionPath[$j], $imageSearchTolerance)
			   If $potionCooldown[0] = 1 Then
				  ; Potion is Ready.
				  For $k = 0 To 2
					 ControlSend($elderScrollsTitle, "", "", $usePotionKey) ; Use Potion
					 ;_SetLogMessage("Used Potion")
					 Sleep(10)
				  Next
			   EndIf
			Next
		 EndIf
	  EndIf
   Else
	  ; Elder Scrolls Window not active
	  Sleep(125)
   EndIf

EndFunc

Func Main()
   GUISetState(@SW_SHOW, $hGUI)
   While 1
	  Update()
	  Sleep(5)
   WEnd

   _OnSynergiesSerialize()
   GUIDelete($hGUI)
EndFunc

Func ClosePressed()
   _OnSynergiesSerialize()
   _OnConfigSerialize()
   GUIDelete($hGUI)
   Exit
EndFunc

Func SetWindowRgn($h_win, $rgn)
    DllCall("user32.dll", "long", "SetWindowRgn", "hwnd", $h_win, "long", $rgn, "int", 1)
EndFunc

;Func CombineRgn(ByRef $rgn1, ByRef $rgn2)
;    DllCall("gdi32.dll", "long", "CombineRgn", "long", $rgn1, "long", $rgn1, "long", $rgn2, "int", 2)
;EndFunc

Func CreateTextRgn(ByRef $CTR_hwnd,$CTR_Text,$CTR_height,$CTR_font="Microsoft Sans Serif",$CTR_weight=1000)
    Local Const $ANSI_CHARSET = 0
    Local Const $OUT_CHARACTER_PRECIS = 2
    Local Const $CLIP_DEFAULT_PRECIS = 0
    Local Const $PROOF_QUALITY = 2
    Local Const $FIXED_PITCH = 1
    Local Const $RGN_XOR = 3
        If $CTR_font = "" Then $CTR_font = "Microsoft Sans Serif"
    If $CTR_weight = -1 Then $CTR_weight = 1000
    Local $gdi_dll = DLLOpen("gdi32.dll")
    Local $CTR_hDC= DLLCall("user32.dll","int","GetDC","hwnd",$CTR_hwnd)
    Local $CTR_hMyFont = DLLCall($gdi_dll,"hwnd","CreateFont","int",$CTR_height,"int",0,"int",0,"int",0, _
                "int",$CTR_weight,"int",0,"int",0,"int",0,"int",$ANSI_CHARSET,"int",$OUT_CHARACTER_PRECIS, _
                "int",$CLIP_DEFAULT_PRECIS,"int",$PROOF_QUALITY,"int",$FIXED_PITCH,"str",$CTR_font )
    Local $CTR_hOldFont = DLLCall($gdi_dll,"hwnd","SelectObject","int",$CTR_hDC[0],"hwnd",$CTR_hMyFont[0])
    DLLCall($gdi_dll,"int","BeginPath","int",$CTR_hDC[0])
    DLLCall($gdi_dll,"int","TextOut","int",$CTR_hDC[0],"int",0,"int",0,"str",$CTR_Text,"int",StringLen($CTR_Text))
    DLLCall($gdi_dll,"int","EndPath","int",$CTR_hDC[0])
    Local $CTR_hRgn1 = DLLCall($gdi_dll,"hwnd","PathToRegion","int",$CTR_hDC[0])
    Local $CTR_rc = DLLStructCreate("int;int;int;int")
    DLLCall($gdi_dll,"int","GetRgnBox","hwnd",$CTR_hRgn1[0],"ptr",DllStructGetPtr($CTR_rc))
    Local $CTR_hRgn2 = DLLCall($gdi_dll,"hwnd","CreateRectRgnIndirect","ptr",DllStructGetPtr($CTR_rc))
    DLLCall($gdi_dll,"int","CombineRgn","hwnd",$CTR_hRgn2[0],"hwnd",$CTR_hRgn2[0],"hwnd",$CTR_hRgn1[0],"int",$RGN_XOR)
    DLLCall($gdi_dll,"int","DeleteObject","hwnd",$CTR_hRgn1[0])
    DLLCall("user32.dll","int","ReleaseDC","hwnd",$CTR_hwnd,"int",$CTR_hDC[0])
    DLLCall($gdi_dll,"int","SelectObject","int",$CTR_hDC[0],"hwnd",$CTR_hOldFont[0])
    DLLClose($gdi_dll)
    Return $CTR_hRgn2[0]
EndFunc

Func _SetLogMessage($msg)
   If $showLogMessage == False Then
	  $showLogMessage = True
	  $begin = TimerInit()
	  $logTextRegion = CreateTextRgn($logHwnd, $msg,50, "Arial", 1000)
	  SetWindowRgn($logHwnd,$logTextRegion)
	  GUISetState(@SW_SHOW, $logHwnd)
	  WinActivate($elderScrollsTitle)
   Endif
EndFunc













